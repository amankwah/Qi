package com.flimbis.qi_core.repository;

import com.flimbis.qi_core.entity.Todo;

import io.reactivex.Single;

public interface TodoRepository extends BaseRepository<Todo> {
    Single<Long> create(Todo todo);

    Single<Long> update(Todo todo);

    Single<Long> delete(long id);
}
