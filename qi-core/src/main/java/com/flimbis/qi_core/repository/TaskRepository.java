package com.flimbis.qi_core.repository;

import com.flimbis.qi_core.entity.Task;

public interface TaskRepository extends BaseRepository<Task>{
}
