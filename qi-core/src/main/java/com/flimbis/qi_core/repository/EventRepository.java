package com.flimbis.qi_core.repository;

import com.flimbis.qi_core.entity.EventEntity;

import io.reactivex.Single;

public interface EventRepository extends BaseRepository<EventEntity>{
    Single<EventEntity> getEvent(String id);
}
