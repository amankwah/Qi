package com.flimbis.qi_core.repository;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.Single;

public interface BaseRepository<T> {
    Observable<List<T>> getAll(int page);

    Single<T> get(long id);
}
