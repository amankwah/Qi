package com.flimbis.qi_core.repository;

import com.flimbis.qi_core.entity.User;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.Single;

public interface UserRepository {
    Observable<User> getUser(String id);

    Observable<List<User>> getAllUsers(String courseId);
}
