package com.flimbis.qi_core.entity;

/*
 * Tasks are Events, TodoList
 * */
public class Task {
    protected int taskId;
    protected String name;
    protected String alias;
    protected String description;
    protected String timestamp;//date

    public Task(int taskId, String alias) {
        this.taskId = taskId;
        this.alias = alias;
    }

    public int getTaskId() {
        return taskId;
    }

    public void setTaskId(int taskId) {
        this.taskId = taskId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }
}
