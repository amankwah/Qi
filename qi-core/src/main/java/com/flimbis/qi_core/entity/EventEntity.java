package com.flimbis.qi_core.entity;

import com.flimbis.qi_core.util.Dated;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

/*
* Events are course schedules, school functions
* */
public class EventEntity{
    private String id;
    private String name;
    private String alias;
    private String host;
    private List<String> venue;
    private String description;
    private String timestamp;
    private String start;
    private String end;
    private int type;
    private int duration;//in mins
    private int isComplete;

    public EventEntity(String id, String name, String alias, String host, List<String> venue, String description, String timestamp, String start, String end, int type, int isComplete) {
        this.id = id;
        this.name = name;
        this.alias = alias;
        this.host = host;
        this.venue = venue;
        this.description = description;
        this.timestamp = timestamp;
        this.start = start;
        this.end = end;
        this.type = type;
        this.isComplete = isComplete;
    }

    public EventEntity() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public List<String> getVenue() {
        return venue;
    }

    public void setVenue(List<String> venue) {
        this.venue = venue;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getStart() {
        return start;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public String getEnd() {
        return end;
    }

    public void setEnd(String end) {
        this.end = end;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public int isComplete() {
        return isComplete;
    }

    public void setComplete(int complete) {
        isComplete = complete;
    }

    public boolean isToday() {
        Date date = null;
        try {
            date = Dated.convertStringToDate(getTimestamp());
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (!Dated.isToday(date)) {
            return false;
        }
        return true;
    }
}
