package com.flimbis.qi_core.entity;

/*
 * user defined activities
 * */
public class Todo extends Task {
    private String id;
    private boolean isComplete;

    public Todo(int taskId, String name, String id) {
        super(taskId, name);
        this.id = id;
        this.isComplete = false;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public boolean isComplete() {
        return isComplete;
    }

    public void setComplete(boolean complete) {
        isComplete = complete;
    }
}
