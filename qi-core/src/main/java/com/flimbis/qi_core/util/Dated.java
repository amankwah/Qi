package com.flimbis.qi_core.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class Dated {
    /*
     *
     * returns weekday Sunday,Monday,...Saturday
     * */
    public static String weekDay(String dateLiteral) {
        Date date = null;
        try {
            date = convertStringToDate(dateLiteral);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (isToday(date)) {
            return "Today";
        }

        if (isYesterday(date)) {
            return "Yesterday";
        }

        return day(dayOfTheWeek(date));
    }

    public static String weekDay2(String dateLiteral) {
        Date date = null;
        try {
            date = convertStringToDate(dateLiteral);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        DateFormat dateFormat = new SimpleDateFormat("EEEE");
        String day = dateFormat.format(date);

        if (isToday(date)) {
            return "Today";
        }

        if (isYesterday(date)) {
            return "Yesterday";
        }

        return day;
    }

    /*
     * returns true if date equals today
     * */
    public static boolean isToday(Date when) {
        Calendar todaysDate = Calendar.getInstance();
        todaysDate.setTime(today());

        Calendar someDate = Calendar.getInstance();
        someDate.setTime(when);

        if ((todaysDate.get(Calendar.DATE) - someDate.get(Calendar.DATE)) != 0)
            return false;

        return true;
    }

    /*
     * returns true if date is directly before today
     * */
    public static boolean isYesterday(Date when) {
        Calendar todaysDate = Calendar.getInstance();
        todaysDate.setTime(today());

        Calendar someDate = Calendar.getInstance();
        someDate.setTime(when);

        if ((todaysDate.get(Calendar.DATE) - someDate.get(Calendar.DATE)) != 1)
            return false;

        return true;
    }

    /*
     * returns week day in numbers 0,1...6
     * */
    public static int dayOfTheWeek(Date when) {
        Calendar c = Calendar.getInstance();
        c.setTime(when);
        int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);

        return dayOfWeek;
    }

    /*
     * returns current date
     * */
    public static Date today() {
        return new Date();
    }

    public static Date convertStringToDate(String dateLiteral) throws ParseException {
        //SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date = simpleDateFormat.parse(dateLiteral);

        return date;
    }

    public static String convertDateToString(Date date) {
        //SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String dated = sdf.format(date);

        return dated;
    }

    public static String fullDate(Date when) {
        SimpleDateFormat sdf = new SimpleDateFormat("EEEE, d MMMM, yyyy");
        String dated = sdf.format(when);

        return dated;
    }

    public static String partDate(Date when) {
        SimpleDateFormat sdf = new SimpleDateFormat("d MMMM, yyyy");
        String dated = sdf.format(when);

        return dated;
    }

    private static String day(int i) {
        switch (i) {
            case 1:
                return "Sunday";
            case 2:
                return "Monday";
            case 3:
                return "Tuesday";
            case 4:
                return "Wednesday";
            case 5:
                return "Thursday";
            case 6:
                return "Friday";
            default:
                return "Saturday";
        }
    }
}
