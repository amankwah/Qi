package com.flimbis.qi_core.interactor;

import com.flimbis.qi_core.entity.Todo;
import com.flimbis.qi_core.repository.TodoRepository;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.Scheduler;

public class GetAllTodos extends UseCase<List<Todo>, Integer> {
    private TodoRepository repository;

    public GetAllTodos(Scheduler uiThread, TodoRepository repository) {
        super(uiThread);
        this.repository = repository;
    }

    @Override
    public Observable<List<Todo>> buildObservable(Integer p) {
        return repository.getAll(p);
    }
}
