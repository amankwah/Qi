package com.flimbis.qi_core.interactor;

import com.flimbis.qi_core.repository.TodoRepository;

import io.reactivex.Observable;
import io.reactivex.Scheduler;

public class DeleteTodo extends UseCase<Long, Long> {
    private TodoRepository repository;

    public DeleteTodo(Scheduler uiThread, TodoRepository repository) {
        super(uiThread);
        this.repository = repository;
    }

    @Override
    public Observable<Long> buildObservable(Long id) {
        return repository.delete(id).toObservable();
    }
}
