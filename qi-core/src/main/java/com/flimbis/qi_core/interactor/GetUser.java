package com.flimbis.qi_core.interactor;

import com.flimbis.qi_core.entity.User;
import com.flimbis.qi_core.repository.UserRepository;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.Scheduler;

public class GetUser extends UseCase<User, String> {
    private UserRepository repository;

    public GetUser(Scheduler uiThread, UserRepository repository) {
        super(uiThread);
        this.repository = repository;
    }

    @Override
    public Observable<User> buildObservable(String p) {
        return repository.getUser(p);
    }
}
