package com.flimbis.qi_core.interactor;

import com.flimbis.qi_core.entity.Task;
import com.flimbis.qi_core.repository.TaskRepository;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.Scheduler;

/*
 * fetch current event or class, and todos
 *
 * */
public class GetRecentTasks extends UseCase<List<Task>, Void> {
    private TaskRepository repository;

    public GetRecentTasks(Scheduler uiThread, TaskRepository repository) {
        super(uiThread);
        this.repository = repository;
    }

    @Override
    public Observable<List<Task>> buildObservable(Void p) {
        return repository.getAll(0);
    }
}
