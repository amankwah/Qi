package com.flimbis.qi_core.interactor;

import com.flimbis.qi_core.entity.EventEntity;
import com.flimbis.qi_core.repository.EventRepository;

import io.reactivex.Observable;
import io.reactivex.Scheduler;

public class GetEvent extends UseCase<EventEntity,Long> {
    private EventRepository repository;

    public GetEvent(Scheduler uiThread, EventRepository repository) {
        super(uiThread);
        this.repository = repository;
    }

    @Override
    public Observable<EventEntity> buildObservable(Long id) {
        return repository.get(id).toObservable();
    }
}
