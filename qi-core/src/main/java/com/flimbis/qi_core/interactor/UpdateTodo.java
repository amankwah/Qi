package com.flimbis.qi_core.interactor;

import com.flimbis.qi_core.entity.Todo;
import com.flimbis.qi_core.repository.TodoRepository;

import io.reactivex.Observable;
import io.reactivex.Scheduler;

public class UpdateTodo extends UseCase<Long, Todo> {
    private TodoRepository repository;

    public UpdateTodo(Scheduler uiThread, TodoRepository repository) {
        super(uiThread);
        this.repository = repository;
    }

    @Override
    public Observable<Long> buildObservable(Todo todo) {
        return repository.update(todo).toObservable();
    }
}
