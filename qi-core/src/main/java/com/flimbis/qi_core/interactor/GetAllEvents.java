package com.flimbis.qi_core.interactor;

import com.flimbis.qi_core.entity.EventEntity;
import com.flimbis.qi_core.repository.EventRepository;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.Scheduler;

public class GetAllEvents extends UseCase<List<EventEntity>, Integer> {
    private EventRepository repository;

    public GetAllEvents(EventRepository repository, Scheduler uiThread) {
        super(uiThread);
        this.repository = repository;
    }

    @Override
    public Observable<List<EventEntity>> buildObservable(Integer p) {
        return repository.getAll(p);
    }
}
