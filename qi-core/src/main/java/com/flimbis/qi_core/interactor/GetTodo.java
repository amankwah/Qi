package com.flimbis.qi_core.interactor;

import com.flimbis.qi_core.entity.Todo;
import com.flimbis.qi_core.repository.TodoRepository;

import io.reactivex.Observable;
import io.reactivex.Scheduler;

public class GetTodo extends UseCase<Todo,Long>{
    private TodoRepository repository;

    public GetTodo(Scheduler uiThread, TodoRepository repository) {
        super(uiThread);
        this.repository = repository;
    }

    @Override
    public Observable<Todo> buildObservable(Long id) {
        return repository.get(id).toObservable();
    }
}
