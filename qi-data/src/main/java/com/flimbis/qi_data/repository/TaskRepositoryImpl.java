package com.flimbis.qi_data.repository;

import com.flimbis.qi_core.entity.Task;
import com.flimbis.qi_core.repository.TaskRepository;
import com.flimbis.qi_data.datasource.TodoDataSource;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.Single;

public class TaskRepositoryImpl implements TaskRepository {
    private TodoDataSource dataSource;

    public TaskRepositoryImpl(TodoDataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public Observable<List<Task>> getAll(int page) {
        return null;
    }

    @Override
    public Single<Task> get(long id) {
        return null;
    }
}
