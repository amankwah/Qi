package com.flimbis.qi_data.repository;

import com.flimbis.qi_core.entity.Todo;
import com.flimbis.qi_core.repository.TodoRepository;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.Single;

public class TodoRepositoryImpl implements TodoRepository {
    @Override
    public Single<Long> create(Todo todo) {
        return null;
    }

    @Override
    public Single<Long> update(Todo todo) {
        return null;
    }

    @Override
    public Single<Long> delete(long id) {
        return null;
    }

    @Override
    public Observable<List<Todo>> getAll(int page) {
        return null;
    }

    @Override
    public Single<Todo> get(long id) {
        return null;
    }
}
