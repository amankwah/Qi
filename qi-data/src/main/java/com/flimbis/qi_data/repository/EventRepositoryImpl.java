package com.flimbis.qi_data.repository;

import com.flimbis.qi_core.entity.EventEntity;
import com.flimbis.qi_core.repository.EventRepository;
import com.flimbis.qi_data.datasource.EventDatasource;
import com.flimbis.qi_data.mapper.EventsMapper;
import com.flimbis.qi_data.model.Events;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.functions.Function;

public class EventRepositoryImpl implements EventRepository {
    private EventDatasource datasource;
    private EventsMapper mapper;

    public EventRepositoryImpl(EventDatasource datasource, EventsMapper mapper) {
        this.datasource = datasource;
        this.mapper = mapper;
    }

    @Override
    public Observable<List<EventEntity>> getAll(int page) {
        Observable<List<EventEntity>> observable = datasource.getAllEvents()
                .map(new Function<List<Events>, List<EventEntity>>() {
                    @Override
                    public List<EventEntity> apply(List<Events> eventsList) throws Exception {
                        return mapper.convertToEventEntityList(eventsList);
                    }
                });
        return observable;
    }

    @Override
    public Single<EventEntity> get(long id) {
        return null;
    }

    @Override
    public Single<EventEntity> getEvent(String id) {
        return null;
    }
}
