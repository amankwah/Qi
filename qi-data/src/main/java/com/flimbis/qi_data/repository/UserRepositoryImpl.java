package com.flimbis.qi_data.repository;

import com.flimbis.qi_core.entity.User;
import com.flimbis.qi_core.repository.UserRepository;
import com.flimbis.qi_data.datasource.UserDatasource;
import com.flimbis.qi_data.mapper.UsersMapper;
import com.flimbis.qi_data.model.Users;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.Single;
import io.reactivex.functions.Function;
import io.reactivex.functions.Predicate;

public class UserRepositoryImpl implements UserRepository {
    private UserDatasource datasource;
    private UsersMapper mapper;

    public UserRepositoryImpl(UserDatasource datasource, UsersMapper mapper) {
        this.datasource = datasource;
        this.mapper = mapper;
    }

    @Override
    public Observable<User> getUser(String id) {
        Observable<User> observable = datasource.getUser(id).map(new Function<Users, User>() {
            @Override
            public User apply(Users users) throws Exception {
                return mapper.convertToUserEntity(users);
            }
        });
        return observable;
    }

    @Override
    public Observable<List<User>> getAllUsers(String courseId) {
        Observable<List<User>> observable = datasource.getUsers()
                .map(new Function<List<Users>, List<User>>() {
                    @Override
                    public List<User> apply(List<Users> users) throws Exception {
                        return mapper.convertToUserEntityList(users);
                    }
                });

        return observable;
    }
}
