package com.flimbis.qi_data.datasource.remote;

import com.flimbis.qi_data.datasource.EventDatasource;
import com.flimbis.qi_data.model.Events;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.Single;

public class EventDatasourceImpl implements EventDatasource {
    private FireDatabaseManager database;
    private ApiService apiService;

    public EventDatasourceImpl(ApiService apiService) {
        this.apiService = apiService;
    }

    @Override
    public Single<Events> getEvent(String id) {
        return null;
    }

    @Override
    public Observable<List<Events>> getAllEvents() {

        Observable<List<Events>> observable = apiService.getAllEvents();
        return observable;
    }

    public interface EventsCallback {
        void callback(List<Events> events);
    }
}
