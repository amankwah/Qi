package com.flimbis.qi_data.datasource.remote;

import android.support.annotation.NonNull;
import android.util.Log;

import com.flimbis.qi_data.model.Events;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class FireDatabaseManager {
    private FirebaseDatabase database;
    private final String KEY_EVENTS = "events";
    private String fdbTag = "FDB_TAG";

    public FireDatabaseManager(FirebaseDatabase database) {
        this.database = database;
    }

    public List<Events> getAllEvents() {
        final List<Events> events = new ArrayList<>();

            database.getReference().child(KEY_EVENTS).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    Log.i(fdbTag, "datachange called");
                    //events.add(dataSnapshot.getValue(Events.class));
                    //iterating through all the nodes
                    for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                        //getting Events
                        Events ev = postSnapshot.getValue(Events.class);
                        //Log.i(fdbTag, "ev-" + ev.getAlias());
                        //adding events to the list
                        events.add(ev);
                    }

                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                    Log.e(fdbTag, "datachange cancelled");

                }
            });


        return events;
    }

    public void getEventsAll(final EventDatasourceImpl.EventsCallback eventsCallback){
        final List<Events> events = new ArrayList<>();
        database.getReference().child(KEY_EVENTS).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Log.i(fdbTag, "datachange called");
                //events.add(dataSnapshot.getValue(Events.class));
                //iterating through all the nodes
                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                    //getting Events
                    Events ev = postSnapshot.getValue(Events.class);
                    //Log.i(fdbTag, "ev-" + ev.getAlias());
                    //adding events to the list
                    events.add(ev);

                }
                eventsCallback.callback(events);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.e(fdbTag, "datachange cancelled");

            }
        });

    }

}
