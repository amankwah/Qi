package com.flimbis.qi_data.datasource.remote;

import android.util.Log;

import com.flimbis.qi_data.datasource.UserDatasource;
import com.flimbis.qi_data.model.Users;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.Single;
import io.reactivex.functions.Function;
import io.reactivex.functions.Predicate;

public class UsersDataSourceImpl implements UserDatasource {
    private ApiService apiService;

    public UsersDataSourceImpl(ApiService apiService) {
        this.apiService = apiService;
    }

    @Override
    public Observable<List<Users>> getUsers() {
        return apiService.getAllUsers();
    }

    @Override
    public Observable<Users> getUser(final String id) {
        Observable<Users> observable = apiService.getUser(id);
        /*Observable<Users> observable = apiService.getAllUsers().
                flatMap(new Function<List<Users>, Observable<Users>>() {
                    @Override
                    public Observable<Users> apply(List<Users> users) throws Exception {
                        return Observable.fromIterable(users);
                    }
                }).
                filter(new Predicate<Users>() {
                    @Override
                    public boolean test(Users users) throws Exception {
                        //Log.i("TAGData",""+users.getId());
                        return users.getId() == id;
                    }
                });*/
        return observable;
    }
}
