package com.flimbis.qi_data.datasource;

import com.flimbis.qi_data.model.Todos;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.Single;

public interface TodoDataSource {
    Single<Todos> get(long id);

    Observable<List<Todos>> getAll(int page);

    Single<Long> create(Todos todo);

    Single<Long> update(Todos todo);

    Single<Long> delete(long id);
}
