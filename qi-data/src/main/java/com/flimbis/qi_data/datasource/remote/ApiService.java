package com.flimbis.qi_data.datasource.remote;

import com.flimbis.qi_data.model.Events;
import com.flimbis.qi_data.model.Users;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface ApiService {
    @GET("events.json")
    Observable<List<Events>> getAllEvents();

    @GET("users.json")
    Observable<List<Users>> getAllUsers();

    @GET("users/{id}.json")
    Observable<Users> getUser(@Path("id") String id);
}
