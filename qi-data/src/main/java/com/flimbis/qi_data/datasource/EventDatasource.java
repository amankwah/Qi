package com.flimbis.qi_data.datasource;

import com.flimbis.qi_data.model.Events;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.Single;

public interface EventDatasource {
    Single<Events> getEvent(String id);

    Observable<List<Events>> getAllEvents();
}
