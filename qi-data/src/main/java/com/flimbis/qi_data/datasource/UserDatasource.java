package com.flimbis.qi_data.datasource;

import com.flimbis.qi_data.model.Users;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.Single;

public interface UserDatasource {
    Observable<List<Users>> getUsers();

    Observable<Users> getUser(String id);
}
