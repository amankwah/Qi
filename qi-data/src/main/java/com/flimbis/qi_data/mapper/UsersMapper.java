package com.flimbis.qi_data.mapper;

import com.flimbis.qi_core.entity.User;
import com.flimbis.qi_data.model.Users;

import java.util.ArrayList;
import java.util.List;

public class UsersMapper {
    private CoursesMapper mapper;

    public UsersMapper(CoursesMapper mapper) {
        this.mapper = mapper;
    }

    public User convertToUserEntity(Users users) {
        User entity = new User();
        entity.setId(users.getId());
        entity.setFirstName(users.getFirstName());
        entity.setLastName(users.getLastName());
        entity.setEmail(users.getEmail());
        entity.setCourses(mapper.convertToCourseEntityList(users.getCourses()));
        entity.setProgram(users.getProgram());
        entity.setPromo(users.getPromo());

        return entity;
    }

    public List<User> convertToUserEntityList(List<Users> usersList) {
        List<User> entityList = new ArrayList<>();
        for (Users users : usersList) {
            entityList.add(convertToUserEntity(users));
        }
        return entityList;
    }
}
