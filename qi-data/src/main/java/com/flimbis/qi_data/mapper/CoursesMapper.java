package com.flimbis.qi_data.mapper;


import com.flimbis.qi_core.entity.Course;
import com.flimbis.qi_data.model.Courses;

import java.util.ArrayList;
import java.util.List;

public class CoursesMapper {
    public CoursesMapper() {
    }

    public Course convertToCourseEntity(Courses courses) {
        Course entity = new Course();
        entity.setId(courses.getId());
        entity.setAlias(courses.getAlias());
        entity.setName(courses.getName());
        entity.setDescription(courses.getDescription());
        entity.setProfessor(courses.getProfessor());

        return entity;
    }

    public List<Course> convertToCourseEntityList(List<Courses> courses) {
        List<Course> courseList = new ArrayList<>();
        for (Courses model : courses) {
            courseList.add(convertToCourseEntity(model));
        }

        return courseList;
    }
}
