package com.flimbis.qi_data.mapper;

import com.flimbis.qi_core.entity.EventEntity;
import com.flimbis.qi_data.model.Events;

import java.util.ArrayList;
import java.util.List;

public class EventsMapper {

    public EventsMapper() {
    }

    public EventEntity convertToEventEntity(Events events) {
        EventEntity entity = new EventEntity();
        entity.setId(events.getId());
        entity.setName(events.getName());
        entity.setAlias(events.getAlias());
        entity.setHost(events.getHost());
        entity.setVenue(events.getVenue());
        entity.setDescription(events.getDescription());
        entity.setTimestamp(events.getTimestamp());
        entity.setStart(events.getStart());
        entity.setEnd(events.getEnd());
        entity.setType(events.getType());
        entity.setComplete(events.isComplete());

        return entity;
    }

    public List<EventEntity> convertToEventEntityList(List<Events> eventsList) {
        List<EventEntity> eventEntityList = new ArrayList<>();
        for (Events events : eventsList) {
            eventEntityList.add(convertToEventEntity(events));
        }

        return eventEntityList;
    }
}
