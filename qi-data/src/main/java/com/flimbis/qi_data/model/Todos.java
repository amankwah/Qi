package com.flimbis.qi_data.model;

public class Todos extends Tasks {
    private String id;
    private boolean isComplete;

    public Todos(int taskId, String name, String alias, String id, boolean isComplete) {
        super(taskId, name, alias);
        this.id = id;
        this.isComplete = isComplete;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public boolean isComplete() {
        return isComplete;
    }

    public void setComplete(boolean complete) {
        isComplete = complete;
    }
}
