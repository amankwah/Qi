package com.flimbis.qi_data.model;


public class Courses {
    private String id;
    private String name;
    private String alias;
    private String description;
    private String professor;
    private int enrollment;

    public Courses() {
    }

    public Courses(String id, String name, String alias) {
        this.id = id;
        this.name = name;
        this.alias = alias;
    }

    public Courses(String id, String name, String alias, String description, String professor) {
        this.id = id;
        this.name = name;
        this.alias = alias;
        this.description = description;
        this.professor = professor;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getProfessor() {
        return professor;
    }

    public void setProfessor(String professor) {
        this.professor = professor;
    }

    public int getEnrollment() {
        return enrollment;
    }

    public void setEnrollment(int enrollment) {
        this.enrollment = enrollment;
    }
}
