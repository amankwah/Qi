package com.flimbis.qi_data.model;

import java.util.List;

public class Events {
    private String id;
    private String name;
    private String alias;
    private String host;
    private List<String> venue;
    private String description;
    private String timestamp;
    private String start;
    private String end;
    private int type;// 0(class) 1(todos)
    private int isComplete; //0(false) 1(true)

    public Events(String id, String name, String alias, String host, List<String> venue, String description, String timestamp, String start, String end, int type, int isComplete) {
        this.id = id;
        this.name = name;
        this.alias = alias;
        this.host = host;
        this.venue = venue;
        this.description = description;
        this.timestamp = timestamp;
        this.start = start;
        this.end = end;
        this.type = type;
        this.isComplete = isComplete;
    }

    public Events() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public List<String> getVenue() {
        return venue;
    }

    public void setVenue(List<String> venue) {
        this.venue = venue;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getStart() {
        return start;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public String getEnd() {
        return end;
    }

    public void setEnd(String end) {
        this.end = end;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int isComplete() {
        return isComplete;
    }

    public void setComplete(int complete) {
        isComplete = complete;
    }
}
