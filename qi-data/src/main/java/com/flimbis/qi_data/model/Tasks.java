package com.flimbis.qi_data.model;

public class Tasks {
    protected int taskId;
    protected String name;
    protected String alias;
    protected String description;
    protected String timestamp;

    public Tasks(int taskId, String name, String alias) {
        this.taskId = taskId;
        this.name = name;
        this.alias = alias;
    }

    public int getTaskId() {
        return taskId;
    }

    public void setTaskId(int taskId) {
        this.taskId = taskId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }
}
