package com.flimbis.qi;

import android.app.Application;

import com.flimbis.qi.di.component.AppComponent;
import com.flimbis.qi.di.component.DaggerAppComponent;
import com.flimbis.qi.di.module.ApiModule;
import com.flimbis.qi.di.module.AppModule;

public class QiApplication extends Application {
    private static QiApplication INSTANCE;
    private static AppComponent appComponent;

    public static QiApplication getInstance() {
        INSTANCE = new QiApplication();
        return INSTANCE;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        getAppComponent();
    }

    public AppComponent getAppComponent() {
        if (appComponent == null) {
            appComponent = DaggerAppComponent.builder()
                    .appModule(new AppModule(this))
                    .apiModule(new ApiModule("https://qifirebaseproject-c45d8.firebaseio.com/"))
                    .build();
        }
        return appComponent;
    }

}