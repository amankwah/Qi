package com.flimbis.qi.util;

import com.flimbis.qi.model.CourseModel;
import com.flimbis.qi_core.entity.Course;

import java.util.ArrayList;
import java.util.List;

public class CourseModelMapper {
    public static CourseModel convertToCourseModel(Course course) {
        CourseModel model = new CourseModel();
        model.setId(course.getId());
        model.setAlias(course.getAlias());
        model.setName(course.getName());
        model.setDescription(course.getDescription());
        model.setProfessor(course.getProfessor());

        return model;
    }

    public static List<CourseModel> convertToCourseModelList(List<Course> courses) {
        List<CourseModel> courseList = new ArrayList<>();
        for (Course entity : courses) {
            courseList.add(convertToCourseModel(entity));
        }

        return courseList;
    }
}
