package com.flimbis.qi.util;

import com.flimbis.qi.model.UserModel;
import com.flimbis.qi_core.entity.User;

import java.util.ArrayList;
import java.util.List;

public class UserModelMapper {
    public static UserModel convertToUserModel(User user) {
        UserModel entity = new UserModel();
        entity.setId(user.getId());
        entity.setFirstName(user.getFirstName());
        entity.setLastName(user.getLastName());
        entity.setEmail(user.getEmail());
        entity.setCourses(CourseModelMapper.convertToCourseModelList(user.getCourses()));
        entity.setProgram(user.getProgram());
        entity.setPromo(user.getPromo());

        return entity;
    }

    public static List<UserModel> convertToUserModelList(List<User> usersList) {
        List<UserModel> modelList = new ArrayList<>();
        for (User users : usersList) {
            modelList.add(convertToUserModel(users));
        }
        return modelList;
    }
}
