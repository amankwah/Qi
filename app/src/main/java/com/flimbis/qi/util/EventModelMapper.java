package com.flimbis.qi.util;

import com.flimbis.qi.model.EventModel;
import com.flimbis.qi_core.entity.EventEntity;

import java.util.ArrayList;
import java.util.List;

public class EventModelMapper {
    public static EventModel convertToEventModel(EventEntity entity) {
        EventModel model = new EventModel();
        model.setId(entity.getId());
        model.setName(entity.getName());
        model.setAlias(entity.getAlias());
        model.setHost(entity.getHost());
        model.setVenue(entity.getVenue());
        model.setDescription(entity.getDescription());
        model.setTimestamp(entity.getTimestamp());
        model.setStart(entity.getStart());
        model.setEnd(entity.getEnd());
        model.setType(entity.getType());
        model.setComplete(entity.isComplete());

        return model;
    }

    public static List<EventModel> convertToEventModelList(List<EventEntity> eventsList) {
        List<EventModel> eventModelList = new ArrayList<>();
        for (EventEntity events : eventsList) {
            eventModelList.add(convertToEventModel(events));
        }

        return eventModelList;
    }
}
