package com.flimbis.qi.ui.events;

import android.util.Log;

import com.flimbis.qi.util.EventModelMapper;
import com.flimbis.qi_core.entity.EventEntity;
import com.flimbis.qi_core.interactor.GetAllEvents;
import com.flimbis.qi_core.interactor.UseCaseObserver;

import java.util.List;

import javax.inject.Inject;

public class EventsPresenter implements EventsViewContract.Presenter {
    private EventsViewContract.View view;
    private GetAllEvents useCase;
    private final String EVPTAG="EVPrTAG";

    @Inject
    public EventsPresenter(EventsViewContract.View view, GetAllEvents useCase) {
        this.view = view;
        this.useCase = useCase;
    }

    @Override
    public void load() {
        useCase.execute(new GetAllEventsUseCaseObservable(),1);
    }

    @Override
    public void unbind() {
        view = null;
        useCase.dispose();
    }

    private class GetAllEventsUseCaseObservable extends UseCaseObserver<List<EventEntity>>{
        @Override
        public void onNext(List<EventEntity> eventEntities) {
            super.onNext(eventEntities);
            Log.i(EVPTAG,"onNext called");
            view.showEvents(EventModelMapper.convertToEventModelList(eventEntities));
        }

        @Override
        public void onError(Throwable e) {
            super.onError(e);
        }

        @Override
        public void onComplete() {
            super.onComplete();
        }
    }
}
