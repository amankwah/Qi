package com.flimbis.qi.ui.courses;

import com.flimbis.qi.model.CourseModel;
import com.flimbis.qi.model.UserModel;
import com.flimbis.qi_data.model.Courses;

import java.util.List;

public interface CourseViewContract {
    interface View {
        void showProgram(UserModel user);

        void showCourses(List<CourseModel> courseModelList);

        void showCourseDetail(CourseModel courseModel);

        void showMessage(String msg);

    }

    interface Presenter {
        void load(String userId);

        void loadCourses(List<CourseModel> courseModelList);

        void navigateToCourseDetail();
    }
}
