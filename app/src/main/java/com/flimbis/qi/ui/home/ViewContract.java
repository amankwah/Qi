package com.flimbis.qi.ui.home;

import com.flimbis.qi.model.EventModel;

import java.util.List;

public interface ViewContract {
    interface View {
        void showDashboard(List<EventModel> eventModelList);


        void showRecentEvents(List<EventModel> eventModelList);

        void showProfile();

        void showEvents();

        boolean isLogin();

        boolean isNetworkAvailable();

        void showMessage(String msg, String type);
    }

    interface Presenter {
        void load();

        void navigateToProfile();

        void navigateToEvents();

        void unBind();
    }
}
