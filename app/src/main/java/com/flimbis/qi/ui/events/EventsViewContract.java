package com.flimbis.qi.ui.events;

import com.flimbis.qi.model.EventModel;
import com.flimbis.qi_core.entity.EventEntity;

import java.util.List;

public interface EventsViewContract {
    interface View {
        void showEvents(List<EventModel> event);

        void showMessage(String msg, String type);
    }

    interface Presenter {
        void load();

        void unbind();
    }
}
