package com.flimbis.qi.ui.home;

import android.util.Log;

import com.flimbis.qi.model.EventModel;
import com.flimbis.qi.util.EventModelMapper;
import com.flimbis.qi_core.entity.EventEntity;
import com.flimbis.qi_core.interactor.GetAllEvents;
import com.flimbis.qi_core.interactor.UseCaseObserver;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

public class MainPresenter implements ViewContract.Presenter {
    private ViewContract.View view;
    private GetAllEvents useCase;
    private final String MNPTAG = "MPrTAG";

    @Inject
    public MainPresenter(GetAllEvents useCase) {
        this.useCase = useCase;
    }

    public void setView(ViewContract.View view) {
        this.view = view;
    }

    @Override
    public void load() {
        useCase.execute(new GetAllEventsUseCaseObservable(), 0);
    }

    @Override
    public void navigateToProfile() {
        view.showProfile();
    }

    @Override
    public void navigateToEvents() {
        view.showEvents();
    }

    @Override
    public void unBind() {
        view = null;
        useCase.dispose();
    }

    private class GetAllEventsUseCaseObservable extends UseCaseObserver<List<EventEntity>> {
        @Override
        public void onNext(List<EventEntity> eventEntities) {
            super.onNext(eventEntities);
            /*if(eventEntities == null || eventEntities.size()==0){
                Log.i("MainPr","null eventEntity");
            }else{*/
                view.showDashboard(EventModelMapper.convertToEventModelList(eventEntities));

                List<EventEntity> eventList = new ArrayList<>();
                for (EventEntity entity : eventEntities) {
                    if (entity.isToday()) {
                        eventList.add(entity);
                    }
                }
                view.showRecentEvents(EventModelMapper.convertToEventModelList(eventList));
            //}

        }

        @Override
        public void onError(Throwable e) {
            super.onError(e);
        }

        @Override
        public void onComplete() {
            super.onComplete();
        }
    }

}
