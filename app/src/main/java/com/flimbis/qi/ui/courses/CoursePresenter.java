package com.flimbis.qi.ui.courses;

import android.util.Log;

import com.flimbis.qi.model.CourseModel;
import com.flimbis.qi.util.CourseModelMapper;
import com.flimbis.qi.util.UserModelMapper;
import com.flimbis.qi_core.entity.User;
import com.flimbis.qi_core.interactor.GetUser;
import com.flimbis.qi_core.interactor.UseCaseObserver;

import java.util.List;

import javax.inject.Inject;

public class CoursePresenter implements CourseViewContract.Presenter {
    private CourseViewContract.View view;
    private GetUser useCase;

    @Inject
    public CoursePresenter(CourseViewContract.View view, GetUser useCase) {
        this.view = view;
        this.useCase = useCase;
    }

    @Override
    public void load(String userId) {
        useCase.execute(new GetUserObserver(), userId);
    }

    @Override
    public void loadCourses(List<CourseModel> courseModelList) {
        view.showCourses(courseModelList);
    }

    @Override
    public void navigateToCourseDetail() {

    }

    class GetUserObserver extends UseCaseObserver<User> {

        @Override
        public void onNext(User user) {
            super.onNext(user);
            if (user != null) {
                //Log.i("TAGCou", "" + user.getFirstName());
                view.showProgram(UserModelMapper.convertToUserModel(user));
            }
        }

        @Override
        public void onError(Throwable e) {
            super.onError(e);
        }

        @Override
        public void onComplete() {
            super.onComplete();
        }
    }
}
