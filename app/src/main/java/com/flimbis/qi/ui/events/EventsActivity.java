package com.flimbis.qi.ui.events;

import android.app.PendingIntent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.flimbis.qi.QiApplication;
import com.flimbis.qi.R;
import com.flimbis.qi.di.component.DaggerEventsComponent;
import com.flimbis.qi.di.component.EventsComponent;
import com.flimbis.qi.di.module.EventsModule;
import com.flimbis.qi.model.EventModel;
import com.flimbis.qi_core.entity.EventEntity;
import com.riontech.calendar.CustomCalendar;
import com.riontech.calendar.dao.EventData;
import com.riontech.calendar.dao.dataAboutDate;
import com.riontech.calendar.utils.CalendarUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.inject.Inject;

public class EventsActivity extends AppCompatActivity implements EventsViewContract.View {
    private CustomCalendar customCalendar;
    private Toolbar toolbar;
    @Inject
    EventsPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppTheme_Def);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_events);

        customCalendar = (CustomCalendar) findViewById(R.id.customCalendar);
        toolbar = findViewById(R.id.toolbar_events);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Events");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        getComponent().inject(this);

        presenter.load();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.events_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                //NavUtils.navigateUpFromSameTask(this);
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right);
    }

    @Override
    public void showEvents(List<EventModel> events) {
        Log.i("EVATAG", "eventlist-");
        for (EventModel ev : events) {
            Log.i("EVATAG", "event-" + ev.getAlias());
            customCalendar.addAnEvent(ev.getTimestamp(), 2, getEventDataList(2, events));
        }

    }

    @Override
    public void showMessage(String msg, String type) {

    }

    private EventsComponent getComponent() {
        EventsComponent component = DaggerEventsComponent.builder()
                .eventsModule(new EventsModule(this))
                .appComponent(QiApplication.getInstance().getAppComponent())
                .build();

        return component;
    }

    public ArrayList<EventData> getEventDataList(int count, List<EventModel> events) {
        ArrayList<EventData> eventDataList = new ArrayList();

        //for (EventModel ev : events) {
        for (int i = 0; i < count; i++) {

            EventData dateData = new EventData();
            ArrayList<dataAboutDate> dataAboutDates = new ArrayList();
            dataAboutDate dataAboutDate = new dataAboutDate();

            dateData.setSection("EURECOM");

            dataAboutDate.setTitle(events.get(i).getAlias());
            dataAboutDate.setSubject(events.get(i).getDescription());

            dataAboutDates.add(dataAboutDate);
            dateData.setData(dataAboutDates);
            eventDataList.add(dateData);
        }

        return eventDataList;
    }

   /* public ArrayList<EventData> getEventDataList(List<EventModel> events) {
        ArrayList<EventData> eventDataList = new ArrayList();

        for (EventModel ev : events) {
            EventData dateData = new EventData();
            ArrayList<dataAboutDate> dataAboutDates = new ArrayList();
            dataAboutDate dataAboutDate = new dataAboutDate();

            dateData.setSection(ev.getName());

            dataAboutDate.setTitle(ev.getName());
            dataAboutDate.setSubject(ev.getDescription());

            dataAboutDates.add(dataAboutDate);
            dateData.setData(dataAboutDates);
            eventDataList.add(dateData);
        }

        return eventDataList;
    }*/
}
