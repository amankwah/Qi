package com.flimbis.qi.ui.courses;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.flimbis.qi.QiApplication;
import com.flimbis.qi.R;
import com.flimbis.qi.adapter.CourseAdapter;
import com.flimbis.qi.di.component.CourseComponent;
import com.flimbis.qi.di.component.DaggerCourseComponent;
import com.flimbis.qi.di.module.CourseModule;
import com.flimbis.qi.model.CourseModel;
import com.flimbis.qi.model.UserModel;

import java.util.List;

import javax.inject.Inject;

/**
 * A simple {@link Fragment} subclass.
 */
public class CourseFragment extends Fragment implements CourseViewContract.View {
    private Context context;
    private CardView cardView;
    private TextView program;
    private TextView promo;
    private TextView name;
    private RecyclerView recyclerView;
    private CourseAdapter adapter;
    @Inject
    CoursePresenter presenter;

    public CourseFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_course, container, false);
        context = getContext();
        initViews(view);
        getComponent().inject(this);

        presenter.load("1");
        // Inflate the layout for this fragment
        return view;
    }

    @Override
    public void showProgram(UserModel user) {
        name.setText(user.getFirstName()+" "+user.getLastName().toUpperCase());
        //name.setText(user.getId().toUpperCase());
        program.setText(user.getProgram());
        promo.setText("Promo-"+user.getPromo());
        presenter.loadCourses(user.getCourses());
    }

    @Override
    public void showCourses(List<CourseModel> courseModelList) {
        adapter = new CourseAdapter(context, courseModelList);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void showCourseDetail(CourseModel courseModel) {

    }

    @Override
    public void showMessage(String msg) {

    }

    private void initViews(View view) {
        program = view.findViewById(R.id.txt_prof_program);
        promo = view.findViewById(R.id.txt_prof_promo);
        name = view.findViewById(R.id.txt_prof_name);
        recyclerView = view.findViewById(R.id.recy_courses);
        LinearLayoutManager layoutManager = new LinearLayoutManager(context);
        recyclerView.setLayoutManager(layoutManager);

    }

    private CourseComponent getComponent() {
        CourseComponent component = DaggerCourseComponent.builder()
                .courseModule(new CourseModule(this))
                .appComponent(QiApplication.getInstance().getAppComponent())
                .build();

        return component;
    }
}
