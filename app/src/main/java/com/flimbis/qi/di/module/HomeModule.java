package com.flimbis.qi.di.module;

import com.flimbis.qi.di.scope.ApplicationScope;
import com.flimbis.qi.di.scope.CustomScope;
import com.flimbis.qi.ui.home.ViewContract;
import com.flimbis.qi_core.interactor.GetAllEvents;
import com.flimbis.qi_core.repository.EventRepository;

import dagger.Module;
import dagger.Provides;
import io.reactivex.Scheduler;

@Module
public class HomeModule {
  /*  private ViewContract.View view;

    public HomeModule(ViewContract.View view) {
        this.view = view;
    }

    @Provides
    @CustomScope
    public ViewContract.View provideView() {
        return view;
    }*/

    @Provides
    @ApplicationScope
    public GetAllEvents provideGetAllEvents(Scheduler uiThread, EventRepository repository) {
        return new GetAllEvents(repository, uiThread);
    }
}
