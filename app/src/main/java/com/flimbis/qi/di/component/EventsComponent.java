package com.flimbis.qi.di.component;

import com.flimbis.qi.di.module.EventsModule;
import com.flimbis.qi.di.scope.CustomScope;
import com.flimbis.qi.ui.events.EventsActivity;

import dagger.Component;

@CustomScope
@Component(modules = EventsModule.class, dependencies = AppComponent.class)
public interface EventsComponent {
    void inject(EventsActivity eventsActivity);
    //expose
}
