package com.flimbis.qi.di.module;

import com.flimbis.qi.di.scope.ApplicationScope;
import com.flimbis.qi_core.repository.UserRepository;
import com.flimbis.qi_data.datasource.UserDatasource;
import com.flimbis.qi_data.datasource.remote.ApiService;
import com.flimbis.qi_data.datasource.remote.UsersDataSourceImpl;
import com.flimbis.qi_data.mapper.CoursesMapper;
import com.flimbis.qi_data.mapper.UsersMapper;
import com.flimbis.qi_data.repository.UserRepositoryImpl;

import dagger.Module;
import dagger.Provides;

@Module
public class UsersDataSourceModule {
    @Provides
    @ApplicationScope
    public UserDatasource provideUserDatasource(ApiService apiService) {
        return new UsersDataSourceImpl(apiService);
    }

    @Provides
    @ApplicationScope
    public CoursesMapper provideCoursesMapper() {
        return new CoursesMapper();
    }

    @Provides
    @ApplicationScope
    public UsersMapper provideUsersMapper(CoursesMapper coursesMapper) {
        return new UsersMapper(coursesMapper);
    }

    @Provides
    @ApplicationScope
    public UserRepository provideUserRepository(UserDatasource datasource, UsersMapper mapper) {
        return new UserRepositoryImpl(datasource, mapper);
    }
}
