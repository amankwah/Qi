package com.flimbis.qi.di.module;

import com.flimbis.qi.di.scope.CustomScope;
import com.flimbis.qi.ui.events.EventsViewContract;
import com.flimbis.qi_core.interactor.GetAllEvents;
import com.flimbis.qi_core.repository.EventRepository;

import dagger.Module;
import dagger.Provides;
import io.reactivex.Scheduler;

@Module
public class EventsModule {
    private EventsViewContract.View view;

    public EventsModule(EventsViewContract.View view) {
        this.view = view;
    }

    @Provides
    @CustomScope
    public EventsViewContract.View provideView() {
        return view;
    }

    @Provides
    @CustomScope
    public GetAllEvents provideGetEventList(Scheduler uiThread, EventRepository repository) {
        return new GetAllEvents(repository, uiThread);
    }
}
