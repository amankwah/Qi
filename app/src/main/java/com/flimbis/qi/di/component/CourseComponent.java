package com.flimbis.qi.di.component;

import com.flimbis.qi.di.module.CourseModule;
import com.flimbis.qi.di.scope.CustomScope;
import com.flimbis.qi.ui.courses.CourseFragment;

import dagger.Component;

@CustomScope
@Component(modules = CourseModule.class, dependencies = AppComponent.class)
public interface CourseComponent {
    void inject(CourseFragment courseFragment);
}
