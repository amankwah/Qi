package com.flimbis.qi.di.component;

import com.flimbis.qi.MainActivity;
import com.flimbis.qi.di.module.ApiModule;
import com.flimbis.qi.di.module.AppModule;
import com.flimbis.qi.di.module.EventsDatasourceModule;
import com.flimbis.qi.di.module.HomeModule;
import com.flimbis.qi.di.module.UsersDataSourceModule;
import com.flimbis.qi.di.scope.ApplicationScope;
import com.flimbis.qi_core.repository.EventRepository;
import com.flimbis.qi_core.repository.UserRepository;
import com.flimbis.qi_data.datasource.remote.ApiService;
import com.flimbis.qi_data.datasource.remote.FireDatabaseManager;

import dagger.Component;
import io.reactivex.Scheduler;

@ApplicationScope
@Component(modules = {AppModule.class, ApiModule.class,HomeModule.class,EventsDatasourceModule.class, UsersDataSourceModule.class})
public interface AppComponent {
    void inject(MainActivity mainActivity);
    //expose
    Scheduler getUiThreadScheduler();
    //FireDatabaseManager getFireDatabaseManager();
    ApiService getApiService();
    EventRepository getEventRepository();
    UserRepository getUserRepository();
}
