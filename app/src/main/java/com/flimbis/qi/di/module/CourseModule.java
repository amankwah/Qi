package com.flimbis.qi.di.module;

import com.flimbis.qi.di.scope.CustomScope;
import com.flimbis.qi.ui.courses.CourseViewContract;
import com.flimbis.qi_core.interactor.GetUser;
import com.flimbis.qi_core.repository.UserRepository;

import dagger.Module;
import dagger.Provides;
import io.reactivex.Scheduler;

@Module
public class CourseModule {
    private CourseViewContract.View view;

    public CourseModule(CourseViewContract.View view) {
        this.view = view;
    }

    @Provides
    @CustomScope
    public CourseViewContract.View provideView(){
        return view;
    }

    @Provides
    @CustomScope
    public GetUser provideGetUserUseCase(Scheduler uithread, UserRepository repository){
        return new GetUser(uithread, repository);
    }
}
