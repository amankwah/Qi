package com.flimbis.qi.di.module;

import com.flimbis.qi.di.scope.ApplicationScope;
import com.flimbis.qi_data.datasource.remote.ApiService;
import com.google.gson.Gson;
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import dagger.Module;
import dagger.Provides;
import retrofit2.CallAdapter;
import retrofit2.Converter;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class ApiModule {
    private String baseUrl;

    public ApiModule(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    @Provides
    @ApplicationScope
    ApiService provideApiService(Retrofit retrofit) {
        return retrofit.create(ApiService.class);
    }

    @Provides
    @ApplicationScope
    Retrofit provideRetrofit(String baseUrl, Converter.Factory converterFactory, CallAdapter.Factory callAdapter) {
        return new Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(converterFactory)
                .addCallAdapterFactory(callAdapter)
                .build();
    }

    @Provides
    @ApplicationScope
    String provideBaseUrl() {
        return baseUrl;
    }

    @Provides
    @ApplicationScope
    public Converter.Factory provideGsonConverterFactory(Gson gson) {
        return GsonConverterFactory.create(gson);
    }

    @Provides
    @ApplicationScope
    public CallAdapter.Factory provideCallAdapterFactory() {
        return RxJava2CallAdapterFactory.create();
    }

    @Provides
    @ApplicationScope
    public Gson provideGson() {
        return new Gson();
    }
}
