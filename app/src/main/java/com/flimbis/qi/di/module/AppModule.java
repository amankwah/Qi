package com.flimbis.qi.di.module;

import android.content.Context;

import com.flimbis.qi.QiApplication;
import com.flimbis.qi.di.qualifier.ApplicationContext;
import com.flimbis.qi.di.scope.ApplicationScope;
import com.flimbis.qi_data.datasource.remote.FireDatabaseManager;
import com.google.firebase.database.FirebaseDatabase;

import dagger.Module;
import dagger.Provides;
import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;

@Module
public class AppModule {
    private QiApplication application;

    public AppModule(QiApplication application) {
        this.application = application;
    }

    @Provides
    @ApplicationScope
    @ApplicationContext
    public Context provideAppContext() { return application;}

  /*  @Provides
    @ApplicationScope
    public FireDatabaseManager provideFireDatabaseManager() {
        return new FireDatabaseManager(FirebaseDatabase.getInstance());
    }*/

    @Provides
    @ApplicationScope
    public Scheduler provideUiScheduler(){ return AndroidSchedulers.mainThread();}
}
