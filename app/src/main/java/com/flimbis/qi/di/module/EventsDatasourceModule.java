package com.flimbis.qi.di.module;

import com.flimbis.qi.di.scope.ApplicationScope;
import com.flimbis.qi_core.repository.EventRepository;
import com.flimbis.qi_data.datasource.EventDatasource;
import com.flimbis.qi_data.datasource.remote.ApiService;
import com.flimbis.qi_data.datasource.remote.EventDatasourceImpl;
import com.flimbis.qi_data.datasource.remote.FireDatabaseManager;
import com.flimbis.qi_data.mapper.EventsMapper;
import com.flimbis.qi_data.repository.EventRepositoryImpl;

import dagger.Module;
import dagger.Provides;

@Module
public class EventsDatasourceModule {
    @Provides
    @ApplicationScope
    public EventDatasource provideEventDatasource(ApiService apiService){
        return new EventDatasourceImpl(apiService);
    }

    @Provides
    @ApplicationScope
    public EventsMapper provideEventsMapper(){
        return new EventsMapper();
    }

    @Provides
    @ApplicationScope
    public EventRepository provideEventRepository(EventDatasource datasource, EventsMapper mapper){
        return new EventRepositoryImpl(datasource, mapper);
    }
}
