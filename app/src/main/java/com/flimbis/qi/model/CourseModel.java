package com.flimbis.qi.model;

import android.os.Parcel;
import android.os.Parcelable;

public class CourseModel implements Parcelable {
    private String id;
    private String name;
    private String alias;
    private String description;
    private String professor;

    public CourseModel() {
    }

    public CourseModel(String id, String name, String alias, String description, String professor) {
        this.id = id;
        this.name = name;
        this.alias = alias;
        this.description = description;
        this.professor = professor;
    }

    protected CourseModel(Parcel in) {
        id = in.readString();
        name = in.readString();
        alias = in.readString();
        description = in.readString();
        professor = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(name);
        dest.writeString(alias);
        dest.writeString(description);
        dest.writeString(professor);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<CourseModel> CREATOR = new Parcelable.Creator<CourseModel>() {
        @Override
        public CourseModel createFromParcel(Parcel in) {
            return new CourseModel(in);
        }

        @Override
        public CourseModel[] newArray(int size) {
            return new CourseModel[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getProfessor() {
        return professor;
    }

    public void setProfessor(String professor) {
        this.professor = professor;
    }

    public static Creator<CourseModel> getCREATOR() {
        return CREATOR;
    }
}