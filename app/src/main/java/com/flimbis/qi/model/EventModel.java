package com.flimbis.qi.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.flimbis.qi_core.util.Dated;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class EventModel implements Parcelable {

    private String id;
    private String name;
    private String alias;
    private String host;
    private List<String> venue;
    private String description;
    private String timestamp;
    private String start;
    private String end;
    private int type;
    private int isComplete;

    public EventModel() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(id);
        parcel.writeString(name);
        parcel.writeString(alias);
        parcel.writeString(host);
        if (venue == null) {
            parcel.writeByte((byte) (0x00));
        } else {
            parcel.writeByte((byte) (0x01));
            parcel.writeList(venue);
        }
        parcel.writeString(description);
        parcel.writeString(timestamp);
        parcel.writeString(start);
        parcel.writeString(end);
        parcel.writeInt(type);
        parcel.writeInt(isComplete);



    }

    public static final Parcelable.Creator<EventModel> CREATOR
            = new Parcelable.Creator<EventModel>() {
        public EventModel createFromParcel(Parcel in) {
            return new EventModel(in);
        }

        public EventModel[] newArray(int size) {
            return new EventModel[size];
        }
    };

    private EventModel(Parcel in) {
        id = in.readString();
        name = in.readString();
        alias = in.readString();
        host = in.readString();
        if (in.readByte() == 0x01) {
            venue = new ArrayList<String>();
            in.readList(venue, String.class.getClassLoader());
        } else {
            venue = null;
        }
        description = in.readString();
        timestamp = in.readString();
        start = in.readString();
        end = in.readString();
        type = in.readInt();
        isComplete = in.readInt();

    }

    public EventModel(String id, String name, String alias) {
        this.id = id;
        this.name = name;
        this.alias = alias;
    }

    public EventModel(String id, String name, String alias, String timestamp) {
        this.id = id;
        this.name = name;
        this.alias = alias;
        this.timestamp = timestamp;
    }

    public EventModel(String id, String name, String alias, String host, List<String> venue, String description, String timestamp, String start, String end, int type, int isComplete) {
        this.id = id;
        this.name = name;
        this.alias = alias;
        this.host = host;
        this.venue = venue;
        this.description = description;
        this.timestamp = timestamp;
        this.start = start;
        this.end = end;
        this.type = type;
        this.isComplete = isComplete;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public List<String> getVenue() {
        return venue;
    }

    public void setVenue(List<String> venue) {
        this.venue = venue;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getStart() {
        return start;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public String getEnd() {
        return end;
    }

    public void setEnd(String end) {
        this.end = end;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int isComplete() {
        return isComplete;
    }

    public void setComplete(int complete) {
        isComplete = complete;
    }

    public boolean isToday() {
        Date date = null;
        try {
            date = Dated.convertStringToDate(getTimestamp());
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (!Dated.isToday(date)) {
            return false;
        }
        return true;
    }
}
