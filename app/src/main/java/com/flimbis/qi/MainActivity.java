package com.flimbis.qi;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.flimbis.qi.adapter.AgendaStreakAdapter;
import com.flimbis.qi.adapter.HomeAdapter;
import com.flimbis.qi.di.component.AppComponent;
import com.flimbis.qi.model.EventModel;
import com.flimbis.qi.ui.events.EventsActivity;
import com.flimbis.qi.ui.home.MainPresenter;
import com.flimbis.qi.ui.home.ViewContract;
import com.flimbis.qi.ui.profile.ProfileActivity;

import java.util.List;

import javax.inject.Inject;

import de.hdodenhof.circleimageview.CircleImageView;

public class MainActivity extends AppCompatActivity implements ViewContract.View {
    private Toolbar toolbar;
    private CircleImageView profileImage;
    private RecyclerView recyclerView;
    private RecyclerView recyAgenda;
    private HomeAdapter adapter;
    private AgendaStreakAdapter agendaAdapter;
    @Inject
    MainPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppTheme_Def);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initViews();
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");

        getApplicationComponent().inject(this);

        presenter.setView(this);

        presenter.load();
    }

    @Override
    protected void onResume() {
        super.onResume();
        profileImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               presenter.navigateToProfile();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_cal:
                Intent intent = new Intent(this, EventsActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.slide_from_right /*enterAnimationId*/, R.anim.slide_to_left/*exitAnimationId*/);
                // presenter.navigateToEvents();
                return true;
          /*  case android.R.id.home:
                presenter.navigateToProfile();
                return true;*/
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void showProfile() {
        Intent intent = new Intent(this, ProfileActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_from_left /*enterAnimationId*/, R.anim.slide_to_right/*exitAnimationId*/);
    }

    @Override
    public void showEvents() {
        Intent intent = new Intent(this, EventsActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_from_right /*enterAnimationId*/, R.anim.slide_to_left/*exitAnimationId*/);
    }

    @Override
    public void showDashboard(List<EventModel> eventModelList) {
        agendaAdapter = new AgendaStreakAdapter(this, eventModelList);
        recyAgenda.setAdapter(agendaAdapter);
    }

    @Override
    public void showRecentEvents(List<EventModel> eventModelList) {
        for(EventModel model: eventModelList){
            if(model.getAlias() == "mobserv"){
                Toast.makeText(this,model.getName(), Toast.LENGTH_SHORT).show();
            }
            Log.i("MainAc",""+model.getAlias());
        }
        adapter = new HomeAdapter(this, eventModelList);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public boolean isLogin() {
        return false;
    }

    @Override
    public boolean isNetworkAvailable() {
        return false;
    }

    @Override
    public void showMessage(String msg, String type) {
        Toast.makeText(this,msg, Toast.LENGTH_SHORT).show();

    }

    private void initViews() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        profileImage = findViewById(R.id.img_prof);
        recyclerView = findViewById(R.id.recy_main);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);

        recyAgenda = findViewById(R.id.recy_agenda);
        LinearLayoutManager horLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        recyAgenda.setLayoutManager(horLayoutManager);
    }

    private AppComponent getApplicationComponent() {
        return QiApplication.getInstance().getAppComponent();
    }

}
