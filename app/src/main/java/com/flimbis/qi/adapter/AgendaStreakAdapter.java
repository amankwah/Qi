package com.flimbis.qi.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.flimbis.qi.R;
import com.flimbis.qi.model.EventModel;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class AgendaStreakAdapter extends RecyclerView.Adapter<AgendaStreakAdapter.ViewHolder>{
    private Context context;
    private List<EventModel> items;
    private String[] day = {"s","m","t","w","t","f","s"};
    private List<String> days = new ArrayList<>();

    public AgendaStreakAdapter(Context context, List<EventModel> items) {
        this.context = context;
        this.items = items;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.items_agenda,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        EventModel model = items.get(position);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private CircleImageView alert;
        private TextView day;
        public ViewHolder(View itemView) {
            super(itemView);

            day = itemView.findViewById(R.id.txt_agenda_day);
            alert = itemView.findViewById(R.id.img_agenda_alert);
        }
    }
}
