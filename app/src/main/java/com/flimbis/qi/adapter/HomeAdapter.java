package com.flimbis.qi.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.flimbis.qi.R;
import com.flimbis.qi.model.EventModel;

import java.util.List;

public class HomeAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{
    private Context context;
    private List<EventModel> items;
    private final int AGENDA = 0;//all
    private final int COURSE = 1;
    private final int TODO_TASK = 2;

    public HomeAdapter(Context context, List<EventModel> items) {
        this.context = context;
        this.items = items;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder;
        LayoutInflater inflater = LayoutInflater.from(context);
        switch (viewType) {
            case COURSE:
                View course_view = inflater.inflate(R.layout.items_main, parent, false);
                viewHolder = new ViewHolder(course_view);
                break;
            case TODO_TASK:
                View todo_view = inflater.inflate(R.layout.items_main_todo, parent, false);
                viewHolder = new TodoViewHolder(todo_view);
                break;
            default:
                View def_view = inflater.inflate(R.layout.items_main, parent, false);
                viewHolder = new ViewHolder(def_view);
                break;
        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        switch (holder.getItemViewType()) {
            case COURSE:
                ViewHolder courseViewHolder = (ViewHolder) holder;
                bindCourseViewHolder(courseViewHolder, position);
                break;
            case TODO_TASK:
                TodoViewHolder todoViewHolder = (TodoViewHolder) holder;
                bindTodoViewHolder(todoViewHolder, position);
                break;
            default:

                break;
        }
    }

    private void bindTodoViewHolder(TodoViewHolder todoViewHolder, int position) {
        EventModel event = items.get(position);

        todoViewHolder.type.setText("GROUP(mobserv)");
        todoViewHolder.title.setText(event.getAlias());
        todoViewHolder.desc.setText(event.getDescription());
    }

    private void bindCourseViewHolder(ViewHolder courseViewHolder, int position) {
        EventModel event = items.get(position);

        courseViewHolder.type.setText("CLASS"+"("+event.getVenue().get(0)+")");
        courseViewHolder.title.setText(event.getAlias());
        courseViewHolder.date.setText("starts~"+event.getStart());//start and end
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public int getItemViewType(int position) {

        //class
        if (items.get(position).getType()== 0) {
            return COURSE;
        } else {
            return TODO_TASK;
        }


    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private TextView type;
        private TextView title;
        private TextView date;
        private CardView card;

        public ViewHolder(View itemView) {
            super(itemView);

            card = itemView.findViewById(R.id.card_main);
            type = itemView.findViewById(R.id.txt_main_type);
            title = itemView.findViewById(R.id.txt_main_title);
            date = itemView.findViewById(R.id.txt_main_date);
        }
    }

    public class TodoViewHolder extends RecyclerView.ViewHolder{
        private TextView type;
        private TextView title;
        private TextView desc;
        private CardView todoCard;

        public TodoViewHolder(View itemView) {
            super(itemView);

            todoCard = itemView.findViewById(R.id.card_main_todo);
            type = itemView.findViewById(R.id.txt_main_td_type);
            title = itemView.findViewById(R.id.txt_main_td_title);
            desc = itemView.findViewById(R.id.txt_main_td_desc);
        }
    }
}
